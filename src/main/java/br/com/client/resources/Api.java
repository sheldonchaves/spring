package br.com.client.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {

	@GetMapping("/list")
	public ResponseEntity<List<String>> search() {

		List<String> ret = new ArrayList<>();

		ret.add("Item1");
		ret.add("Item2");
		ret.add("Item3");
		ret.add("Item4");
		ret.add("Item5");

		return new ResponseEntity<List<String>>(ret, HttpStatus.OK);
	}

}
